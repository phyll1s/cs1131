import animator.*;
import java.awt.*;
/**
 * This creates the rider 
 * @author Andrew Markiewicz
 *
 */
public class Rider extends Animated {

	public void draw(int x, int y)	{
		
		screen.setColor(Color.red);
		screen.fillOval(x+10, y-15, 10, 20);
		//Creates the rider and colors it red
	}
}
