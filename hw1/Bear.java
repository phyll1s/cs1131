import animator.*;
import java.awt.*;
/**
 * This creates the first Bear
 * @author Andrew Markiewicz
 *
 */
public class Bear extends Animated {
	
	private int startx1 = 10;
	private int starty1 = 400;
	private int startx2 = 200;
	private int starty2 = 10;
	//Variables for xy starting and movement
	public void draw()	{
		
		
		startx1 = (startx1 + 7);
		starty1 = (starty1 - 7);
		startx2 = (startx2 - 5);		
		starty2 = (starty2 + 5);
		//Movement +- xy
		
		if(starty1 <= 0){
			starty1 = 400;
			startx1 = 10;
		}
		if(startx2 <= 0){
			startx2 = 200;
			starty2 = 10;
		}
		//"Rubberbands" bears moving
		
		screen.setColor(Color.darkGray);
		screen.fillOval(startx1, starty1, 30, 30);
		screen.fillOval(startx2, starty2, 30, 30);

		//Draws 'bears'
	}
}
