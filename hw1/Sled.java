import animator.*;
import java.awt.*;
/**
 * This class will create the sled that will be move-able with the arrow keys
 * @author Andrew Markiewicz
 *
 */
public class Sled extends Animated {
	
	public void draw(int x, int y)	{
		
		screen.setColor(Color.LIGHT_GRAY);
		screen.fillRect(x, y, 40, 10);
		//Creates the sled and colors it gray
	}
	

}
