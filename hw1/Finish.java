import animator.*;
import java.awt.*;
/**
 * This creates the finish place
 * @author Andrew Markiewicz
 *
 */
public class Finish extends Animated {
	
	String str1 = "FINISH";
	public void draw()	{
		
		
		screen.setColor(Color.black);
		screen.drawString(str1, 20, 20);
		screen.fillRect(20, 30, 40, 10);
		//Creates the finish line / sign
	}
		
}
