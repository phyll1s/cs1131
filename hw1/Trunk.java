import animator.*;
import java.awt.*;
/**
 * This will create the Trunks of the trees
 * @author Andrew Markiewicz
 *
 */
public class Trunk extends Animated {
	
	public void draw()	{
		
		screen.setColor(Color.black);
		screen.fillRect(200, 450, 15, 25);
		screen.fillRect(600, 450, 15, 25);
		screen.fillRect(300, 250, 15, 25);
		//Draws all the trunks
	}

}
