import animator.*;
import java.awt.*;
/**
 * This class, Scene, will call the classes: Tree, Trunk, Sled, and Bear.
 * @author Andrew Markiewicz
 *
 */
public class Scene extends Animated {
	
	public void startup()	{
		
		Finish sign = new Finish();
		animator.include(sign);
		//creates object sign from the Finish class and includes it in the animator
		
		Trunk trunks = new Trunk();
		animator.include(trunks);
		//creates object trunks from the Trunk class and includes it in the animator
		
		Tree treetops = new Tree();
		animator.include(treetops);
		//creates object treetops from the Tree class and includes it in the animator
		
		Sled sled1 = new Sled();
		animator.include(sled1);
		//creates object sled1 from the Sled class and includes it in the animator
		
		Rider rider1 = new Rider();
		animator.include(rider1); 
		//creates object rider1 from the Rider class and includes it in the animator
						
		Bear bear1 = new Bear();
		animator.include(bear1);
		//creates object bear1 from the Bear class and includes it in the animator

	}

}
