import javax.swing.*;
import java.awt.*;


/**
 * The GUI of the Snow Accumulator project
 * 
 * @author AJMARKIE * 
 * 
 */
public class GUIBox  {
	
	/** Static variable for the total snow accumulated */
	public static float total = 0;

	
	/**
	 * The main method to create a new GUIBox object
	 * 
	 * @param args
	 */
	public static void main(String [] args){
		new GUIBox(); 
	}

	
	/**
	 *  Creates the windows, panels, and sets the layouts. Then Draws them all 
	 * 
	 * 
	 */
	public GUIBox(){

		//Creates the window
		JFrame window = new JFrame();                           
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		window.setTitle("Snow Accumulator");
		window.setVisible(true); 

		//Creates a new JPanel
		JPanel content = new JPanel(); 

		//Adds the content panel to the window
		window.add(content);

		//Creates panels 0 to 3 (from top to bottom)
		JPanel row0 = new JPanel();
		JPanel row1 = new JPanel();
		JPanel row2 = new JPanel();
		JPanel row3 = new JPanel();

		//Adds the rows to the content panel
		content.add(row0);
		content.add(row1);
		content.add(row2);
		content.add(row3);

		//Sets the layouts of the panels		
		//Sets the Content panel's layout to a grid layout with 4 rows and 1 column
		content.setLayout(new GridLayout(4,1)); 

		//Flow layout for the row's panels
		row0.setLayout(new FlowLayout());
		row1.setLayout(new FlowLayout());
		row2.setLayout(new FlowLayout());
		row3.setLayout(new FlowLayout());

		//creates new JLabels with needed text
		JLabel inputText = new JLabel("How much snow since the last measurement (in Inches)?");
		JLabel lastUpdate = new JLabel("Last Update:");
		JLabel current = new JLabel("Current Total Snowfall: ");
		JLabel totalText = new JLabel(total + "");
		JLabel dateText = new JLabel();

		//adds the JLabels to the rows
		row0.add(inputText);
		row1.add(lastUpdate);
		row1.add(dateText);
		row2.add(current);
		row2.add(totalText);

		//Creates the JButtons
		JButton clear = new JButton("Clear");
		JButton exit = new JButton("Exit");

		//Adds the buttons to bottom row
		row3.add(clear);
		row3.add(exit);

		//Creates the text field to input the snow
		JFormattedTextField inputBox = new JFormattedTextField(new Float(0));
		inputBox.setColumns(7);	
		inputBox.setText("0.0");

		//Adds the JFormattedTextField the top row
		row0.add(inputBox);

		//Creates the backend object
		Backend backend = new Backend(inputBox, clear, exit, totalText, dateText);

		//Adds action listeners to the buttons and textfield
		clear.addActionListener(backend);
		exit.addActionListener(backend);
		inputBox.addActionListener(backend);

		//Packs the window to draw everything and give appropriate size
		window.pack();
	}
}