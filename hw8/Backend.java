import java.util.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 
 * The Backend of the Snow Accumulator project. Mostly listeners 
 * 
 * @author AJMARKIE
 *
 */
public class Backend implements ActionListener {

	/**An instance reference to the input field in GUIBox */
	JFormattedTextField inputBox;
	
	/**An instance reference to the clear and exit buttons */
	JButton clear, exit;
	
	/**An instance reference to the output of the total and the date */
	JLabel totalText, dateText;

	/** Calendar object used to the get the time. Initialized to the current time */
	Calendar c = Calendar.getInstance();


	/** 
	 * Constructor for the Backend class - assigns instance references to the recieved references 
	 *
	 *@param tf The received reference to the input text field
	 *@param b1 The received reference to the clear button
	 *@param b2 The received reference to the exit button
	 *@param l1 The received reference to the total text output label
	 *@param l2 The received reference to the date label     
	 *     
	 */     
	public Backend(JFormattedTextField tf, JButton b1, JButton b2, JLabel l1, JLabel l2 ){
		inputBox = tf;
		clear = b1;
		exit = b2;		
		totalText = l1;
		dateText = l2;
	}

	
	/**
	 * The action listener of the clear and exit buttons, and the action listener of the input field
	 *  
	 */
	public void actionPerformed(ActionEvent e) {

		//If the clear button is pressed, empty the total snowfall
		if(e.getSource() == clear){
			//sets total to 0 and updates the text
			GUIBox.total = 0;
			totalText.setText(GUIBox.total + "");

			//sets the clock to the current time
			c = Calendar.getInstance();
			dateText.setText(c.getTime() + "");

			//Sets the text field to 0.0
			inputBox.setValue(new Float(0.0));
			inputBox.setText("0.0");
		}

		// If the exit button is pressed, quit the application
		if(e.getSource() == exit)
			System.exit(0);

		//If the enter is pressed after entering a value in for the input box
		//JFormattedTextField automatically clears unwanted char's
		if( e.getSource() == inputBox){

			//gets the current value of the textbox
			float amount = ((Float)inputBox.getValue()).floatValue();

			//updates total and displays it
			GUIBox.total = GUIBox.total + amount;
			totalText.setText(GUIBox.total + "");
			inputBox.setText(amount + "");

			//updates the time and displays it
			c = Calendar.getInstance();
			dateText.setText(c.getTime() + "");

		}
	}
}
