import animator.*;
import java.awt.*;
/**
 * This will create the Trunks of the trees
 * @author Andrew Markiewicz
 *
 */
public class Trunk extends Animated {
		
		private final int sizex = 15;
		private final int sizey = 25;
		//Declares x & y values for size of trunks
		
	public void draw()	{
				
		
		final int xLocal = animator.getSceneWidth();
		final int yLocal = animator.getSceneHeight();
		//Sets the variables x&y Local to t he scene width and height for use in making the trunks
	
		screen.setColor(Color.black);
		screen.fillRect(xLocal / 2, (int) (yLocal * .22), sizex, sizey);
		screen.fillRect(xLocal / 2 + 180, (int) (yLocal * .45), sizex, sizey);
		screen.fillRect(xLocal / 2 - 200, (int) (yLocal *  .5), sizex, sizey);
		screen.fillRect(xLocal / 2 - 340, (int) (yLocal *  .6), sizex, sizey);
		screen.fillRect(xLocal / 2 + 40, (int) (yLocal *  .7), sizex, sizey);
		
		//Draws all the trunks with respect to the scen size +- for placement
	}

}
