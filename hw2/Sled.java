import animator.*;
import java.awt.*;
/**
 * This class will create the sled that will be move-able with the arrow keys
 * @author Andrew Markiewicz
 *
 */
public class Sled extends Animated {
	int xPos;
	int yPos;
	//Create x&y pos variables for sled position
	public void draw( int x, int y)	{

		xPos = x;
		yPos = y;
		
		screen.setColor(Color.LIGHT_GRAY);
		screen.fillRect(x, y , 40, 10);
		//Creates the sled and colors it gray
	}
	

}
