import animator.*;
import java.awt.*;
/**
 * This creates the first Bear
 * @author Andrew Markiewicz
 *
 */
public class Bear extends Animated {

		private final int diameter = 30;
		int xLocal = 0;
		int xChase = 0;
		int yChase = 0;
		int sledxp;
		int sledyp;
		Sled sledp;
		//Sets all variables needed
public Bear(Sled sled1)	{
	
	sledp = sled1;
}
		//ends constructor that passes sled instance to bear instance
		
	public void draw()	{

		sledxp = sledp.xPos;
		sledyp = sledp.yPos;
		//passes the x&y position to the bear instance
		xLocal= ((xLocal + 15) %animator.getSceneWidth()); 
		//Set the xLocal to increase and wrap around screen
		final int yLocal = animator.getSceneHeight();
		
		screen.setColor(Color.GRAY);
		screen.fillOval(xLocal , yLocal - 80 , diameter, diameter);
		//Draws bottom bear
		
		if(sledxp > xChase){
			xChase = xChase + 5;
		}
		
		else{
			xChase = xChase -5;
		}
		
		if(sledyp > yChase){
			yChase = yChase + 5;
		}
		
		else{
			yChase = yChase - 5;
		}
		//Pretty much makes the bear chase the sled by increasing/decreasing the speed of the bear dependeding on where it is in relation to the sled
		
		screen.fillOval(xChase,yChase, diameter, diameter);  //draws the chasing bear
	}
}
