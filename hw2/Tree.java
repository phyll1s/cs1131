import animator.*;
import java.awt.*;
/**
 * This will create the Leaves of the trees
 * @author Andrew Markiewicz
 *
 */
public class Tree extends Animated {

		private final int diameter = 50;
		//Set diameter value
	
	public void draw()	{
		
		final int xLocal = animator.getSceneWidth();
		final int yLocal = animator.getSceneHeight();
		//Sets variables x&y Local to use from the scene width and height
		
		screen.setColor(Color.green);
		screen.fillOval(xLocal / 2 - 15, (int) (yLocal * .14), diameter, diameter);
		screen.fillOval(xLocal / 2 + 165, (int) (yLocal * .37), diameter, diameter);
		screen.fillOval(xLocal / 2 - 215, (int) (yLocal * .42), diameter, diameter);
		screen.fillOval(xLocal / 2 - 355, (int) (yLocal * .52), diameter, diameter);
		screen.fillOval(xLocal / 2 + 25, (int) (yLocal * .62), diameter, diameter);
		
		//Draws all the tree tops with respect to the scen size. -+ some distance to fit in place
		
	}
}
