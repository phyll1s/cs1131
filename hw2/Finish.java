import animator.*;
import java.awt.*;
/**
 * This creates the finish place
 * @author Andrew Markiewicz
 *
 */
public class Finish extends Animated {
	
	int xLocal;
	int xpos;
	int ypos;
	Sled sled1;	//creates sled1 variable of Sled type
	
	
	String str1 = "FINISH";
	
	public Finish(Sled sledx)	{
		sled1 = sledx;
		
	}
	//End constructor that passes the instance of the sled object to the finish object
	public void draw()	{
		
		xpos = sled1.xPos;
		ypos = sled1.yPos;
		//passes the x and y values from the sled instance to the finish instance
		
		int xWidth = animator.getSceneWidth();
		int yLocal = animator.getSceneHeight();
		//Sets the variables x&y Local to the scene width and height
	
		if(xpos <= (xWidth - xpos + 20) && xpos >= (xWidth - xpos - 10) && (int)(yLocal *.96 -2) <= ypos)	{
			screen.setColor(Color.green);
			screen.fillRect(0, 0, animator.getSceneWidth(), animator.getSceneHeight());
		}
		//Displays a green screen if the the sled comes into close enough vicinity of the finish square

		screen.setColor(Color.black);
		screen.drawString(str1, xWidth - xpos,  (int) (yLocal * .98)); //Draws the word 'finish'
		screen.drawRect(xWidth - xpos,(int) (yLocal * .96 - 2), 50, 20); //Draws box around finish
		screen.drawRect(0, yLocal-30, xWidth, 30); // Draws the finish 'zone'

		
	}
		
}
