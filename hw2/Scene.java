import animator.*;
import java.awt.*;
/**
 * This class, Scene, will call the classes: Tree, Trunk, Sled, and Bear.
 * @author Andrew Markiewicz
 *
 */
public class Scene extends Animated {
	

	
	public void startup()	{
		
		Sled sled1 = new Sled();	//defines the sled1 object
		Bear bears = new Bear(sled1);	//Defines the bears object
	
		
		
		Finish sign = new Finish(sled1);
		animator.include(sign);		//Defines and includes the sign object
	
		
		Trunk trunks = new Trunk();
		animator.include(trunks);	//Defines and incldues the trunks object
		
		
		Tree treetops = new Tree();
		animator.include(treetops);	//Defines the treetops object, and includes it in the animator

		
		
		animator.include(sled1);	//Includes the sled1 object in the animator
	
		
		
		Rider rider1 = new Rider();
		animator.include(rider1); 	//defines the rider1 object, and includes it in the animator

		
		animator.include(bears);	//Includes the bears object in the animator


	}
	


}
