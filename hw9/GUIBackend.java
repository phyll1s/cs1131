import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.*;

public class GUIBackend extends AbstractTableModel implements ActionListener {


	//Creates references to received GUI Items
	private JMenuItem open;
	private JMenuItem save;
	private JMenuItem saveas;
	private JMenuItem exit;
	private JTextField inputField;
	private JButton okButton;
	private JSpinner spinner;
	private Component window;
	private JLabel total;
	private JLabel average; 
	private JLabel snowdays;
	private JLabel totalTime;
	
	//Creates a reference to a file
	private File f;
	
	//Creates an instance of the snowdata class
	SnowData data = new SnowData();

	//Creates two vectors that hold the table column header and the data in the table
	Vector<String> tableHeaders;
	Vector<Vector<Object>> tableData;

	/**
	 * Constructor to get all the references to the needed GUI objects
	 * @param l0
	 * @param l1
	 * @param l2
	 * @param l3
	 * @param o
	 * @param i0
	 * @param i1
	 * @param i2
	 * @param i3
	 * @param f0
	 * @param b0
	 * @param s0
	 */
	public GUIBackend(JLabel l0, JLabel l1, JLabel l2, JLabel l3, Component o, JMenuItem i0, JMenuItem i1, JMenuItem i2, JMenuItem i3, JTextField f0, JButton b0, JSpinner s0){

		window = o;
		open = i0;
		save = i1;
		saveas = i2;
		exit = i3; 
		inputField = f0;
		okButton = b0;
		spinner = s0;
		
		total = l0;
		average = l1;
		snowdays = l2;
		totalTime = l3;
		

		//Initializes an array for the headers of the table
		tableHeaders = new Vector<String>();
		tableHeaders.add("Date");
		tableHeaders.add("Accumulation (Inches)");

		//Initializes an array for the data entered
		tableData = new Vector<Vector<Object>>();
		this.prepTable();
		this.fireTableDataChanged();
		this.updateLabels();

	}




	/**
	 * Method to grab the actions and do what is needed
	 */
	public void actionPerformed(ActionEvent e) {


		//If open is pressed, pass in file to open
		if(e.getSource() == open){
			
			final JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(window);

			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = chooser.getSelectedFile();
				data.readFromFile(f);
				this.prepTable();
				this.fireTableDataChanged();
				this.updateLabels();
			}
		}

		//If save is pressed, save file
		if(e.getSource() == save){

			try{
				data.writeToFile(f);

				//If there is no file chosen yet, open file save dialog
			} catch(NullPointerException n) {

				final JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showSaveDialog(window);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					f = chooser.getSelectedFile();
					data.writeToFile(f);
				}
			}
		}


		//If save as is pressed, pass in file to save
		if(e.getSource() == saveas){
			final JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showSaveDialog(window);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = chooser.getSelectedFile();
				data.writeToFile(f);
			}				
		}


		//If exit is pressed, close
		if(e.getSource() == exit)
			System.exit(0);


		//Ok is pressed or enter is pressed after entering data
		if(e.getSource() == okButton || e.getSource() == inputField){

			double inches = 0;

			try{
				inches = Double.parseDouble(inputField.getText());
			} catch(NumberFormatException d) {}
			data.addDay((Date)(spinner.getValue()),inches);

			this.prepTable();
			this.fireTableDataChanged();
			this.updateLabels();

		}

	}


	/**
	 * Enters all the data into the table vectors - clearing them first and sorting linked list
	 */
	public void prepTable(){

		data.sort();
		tableData.clear();
		int counter = 0;
		for(int i = 0; i<SnowData.days.size(); i++){
			tableData.add(new Vector<Object>());
		}
		for(SnowDay d : SnowData.days){	

			tableData.elementAt(counter).add(d.getDate());
			tableData.elementAt(counter).add(d.getSnow());
			counter++;
		}


	}
	
	/**
	 * Updates all the labels when values are updated
	 */
	public void updateLabels(){
		
		total.setText(String.format("%7.3f", data.getTotal()));
		snowdays.setText(String.format("%7.3f", data.getSnowDays()));
		totalTime.setText("" + data.totalDays());
		double temp = (double) data.getTotal() / data.totalDays();
		average.setText(String.format("%7.3f", temp));
				
	}


	/**
	 * Gets the name of a column
	 */
	public String getColumnName(int col) {
		return tableHeaders.elementAt(col);
	}	 	
	public int getColumnCount() {
		return tableHeaders.size();
	}
	public int getRowCount() {
		return tableData.size();
	}
	
	/**
	 * Gets the values at certain places in the table
	 */
	public Object getValueAt(int row, int col) {
		String toReturn;
		//Formats correctly
		if(col == 0)
			toReturn = String.format("%tD", tableData.elementAt(row).get(col));
		else
			toReturn = String.format("%7.3f", tableData.elementAt(row).get(col));

		return toReturn;
	}


}
