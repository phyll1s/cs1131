import java.awt.*;
import javax.swing.*;
import java.util.*;

public class GUI {

	/**
	 * Used to create an instance of the GUI
	 * @param args
	 */
	public static void main(String [] args){
		new GUI(); 
	}

	
	public GUI(){
		
		
		//Creates the window
		JFrame window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		window.setTitle("Snow Day Calculator");
		window.setVisible(true); 
		window.setLayout(new BorderLayout());
		
		//creates gui 'parts'
		JPanel panel1 = new JPanel();		
		JMenuBar bar = new JMenuBar();		
		JMenu menu = new JMenu("File");
		
		//Creates the menu options
		JMenuItem open = new JMenuItem("Open");
		JMenuItem save = new JMenuItem("Save");
		JMenuItem saveas = new JMenuItem("Save As");
		JMenuItem exit = new JMenuItem("Exit");
		
		//Creates the input text field
		JTextField inputField = new JTextField(7);
		
		//Creates the OK button
		JButton okButton = new JButton("Ok");
		

		//creates the date spinner
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		//add's a milisec to set the end date ahead of current
		calendar.add(Calendar.MILLISECOND, 1);
		Date endDate = calendar.getTime();
		//sets date to the correct begining date
		calendar.set(1970, 0, 0);
		Date startDate = calendar.getTime();
		
		//Creates the JSpinner and sets the model to mm/dd/yyyy
		SpinnerModel model = new SpinnerDateModel(now, startDate, endDate, Calendar.YEAR);
		JSpinner spinner = new JSpinner(model);
		spinner.setEditor(new JSpinner.DateEditor(spinner, "MM/dd/yyyy"));
		
		//Creates the needed Labels 
		JLabel totalLabel  = new JLabel("Total:");
		JLabel totalDataLabel  = new JLabel("");
		JLabel averageLabel = new JLabel("Average:");
		JLabel averageDataLabel = new JLabel("");
		JLabel snowDaysLabel = new JLabel("Snow Days:");
		JLabel snowDaysDataLabel = new JLabel("");
		JLabel totalTimeLabel = new JLabel("Total Time");
		JLabel totalTimeDataLabel = new JLabel("");
		
		
		//Creates the backend object and passes it needed variables
		GUIBackend backend = new GUIBackend(totalDataLabel, averageDataLabel, snowDaysDataLabel, totalTimeDataLabel, window, open, save, saveas, exit, inputField, okButton, spinner);

		
		//Creates the table
		JTable table = new JTable(backend);
		JScrollPane pane1 = new JScrollPane(table);
		
		//Creates the three other needed panels
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		
		//Creates the labels needed
		JLabel dateLabel = new JLabel("Date");
		JLabel snowFallLabel = new JLabel("Snowfall");
		JLabel inchesLabel = new JLabel("Inches");
		
	
		//sets layouts
		panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		panel2.setLayout(new GridLayout(2,1));
		panel3.setLayout(new FlowLayout());
		panel4.setLayout(new GridLayout(2,4));
		
		//Adds objects in the right places
		window.add(panel1, BorderLayout.NORTH);
		panel1.add(bar);		
		
		bar.add(menu);
		menu.add(open);
		menu.add(save);
		menu.add(saveas);
		menu.add(exit);
		
		window.add(pane1, BorderLayout.CENTER);
		window.add(panel2, BorderLayout.SOUTH);
		
		panel2.add(panel3);
		panel2.add(panel4);
		
		panel3.add(dateLabel);
		panel3.add(spinner);
		panel3.add(snowFallLabel);
		panel3.add(inputField);
		panel3.add(inchesLabel);
		panel3.add(okButton);
		
		panel4.add(totalLabel);
		panel4.add(totalDataLabel);
		panel4.add(averageLabel);
		panel4.add(averageDataLabel);
		panel4.add(snowDaysLabel);
		panel4.add(snowDaysDataLabel);
		panel4.add(totalTimeLabel);
		panel4.add(totalTimeDataLabel);
		
		//Shapes and sizes the window
		window.pack();		
		
		//Add's action listeners
		open.addActionListener(backend);
		save.addActionListener(backend);
		saveas.addActionListener(backend);
		exit.addActionListener(backend);
		inputField.addActionListener(backend);
		okButton.addActionListener(backend);
			
	}
		
}
