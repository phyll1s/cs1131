import java.util.Date;

/**
 * 
 * The class for keeping track of one snow day
 * 
 * @author AJMARKIE
 *
 */
public class SnowDay implements Comparable<SnowDay> {

	private double accumulation;

	//Create a new date object
	Date date = new Date();

	/**
	 * Constructor that sets the date and accumulation
	 * @param d The date
	 * @param s The snow that day
	 */
	SnowDay(Date d, double s){

		date = d;
		accumulation = s;

	}

	/**
	 * Sets the date
	 * @param d Date value to set
	 */
	public  void setDate(Date d){
		date = d;
	}

	/**
	 * Sets the snow value
	 * @param d The amount of snow to set
	 */
	public void setSnow(double d){
		accumulation = d;
	}

	/**
	 *Returns this date 
	 * @return returns this date
	 */
	public Date getDate(){
		return date;
	}

	/**
	 * Returns the amount of snow this day
	 * @return returns this day's snowfall
	 */
	public double getSnow(){
		return accumulation;
	}


	/**
	 * Gets the date of the day passed to it and compares it to this date
	 * @param anotherDate The date of the passed day
	 */
	public int compareTo(SnowDay anotherDay){
		Date anotherDate = anotherDay.getDate();
		return date.compareTo(anotherDate);
	}

	/**
	 * Formats the toString
	 */
	public String toString(){
		return String.format("%tD %7.3f%n", date, accumulation);
	}

}
