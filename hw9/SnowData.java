import java.io.*;
import java.util.*;

/**
 * Class to Tie together all the snow data
 *  * @author AJMARKIE
 */
public class SnowData {

	//Linked list of all the days with snow
	static LinkedList<SnowDay> days = new LinkedList<SnowDay>();

	/**
	 * @return returns the amount of days entered so far
	 */
	public int size(){
		return days.size();
	}

	/**
	 * Adds a snow day 
	 * @param day the date of the new day
	 * @param snow the amount of snow that day
	 */
	public void addDay(Date day, double snow){
		//Creates the day to add
		SnowDay toAdd = new SnowDay(day, snow);
		
		boolean equals = false;
		SnowDay tempDay= null;
		
		//Checks to see if the day to add is already in the linked list
		for(SnowDay d : days){			
			if(String.format("%tD", toAdd.getDate()).equals(String.format("%tD", d.getDate()))){
				tempDay = d;
				equals = true;
			}		
		}
		
		//If the snow day to add already exists in the linked list, update it, else add the new day
		if(equals){
			tempDay.setDate(day);
			tempDay.setSnow(snow);			
		} else {
		days.add(toAdd);
		}

	}

	/**
	 * gets the number of days with snow
	 * @return returns total days with snow > 0
	 */
	public double getSnowDays(){
		int counter = 0;
		//Traverses through the linked list, counts up if the day has snow
		for(SnowDay d : days){			
			if ( d.getSnow() > 0){
				counter++;		
			}
		}
		return counter;
	}

	/**
	 * gets the total amount of snowfall
	 * @return returns total snowfall
	 */
	static double getTotal(){
		double total = 0;
		for(SnowDay d : days){
			total+= d.getSnow();
		}
		return total;
	}

	/**
	 * Reads from the file passed and adds days in the file to the Linked List
	 * @param f The file passed into the reader
	 */
	@SuppressWarnings("deprecation")
	public void readFromFile(File f){
		Scanner sc = null;
		
		//Try catch just incase the file is not there
		try {
			sc = new Scanner(f);
		} catch (FileNotFoundException e) {
			System.out.println("Pick a real file...");
		}

		//Adds new days to the linked list as long as there are values
		while(sc.hasNext()){
			String s = sc.next();
			Date d = new Date(Date.parse(s));
			double snow = sc.nextFloat();
			this.addDay(d, snow);
		}

	}


	/**
	 * Writes all days in the linked list to the file specified
	 * @param f The file to write to
	 */
	public void writeToFile(File f){
		PrintWriter writer = null;
		
		//Try block around just in case the file selected cannot be written to
		try {
			writer = new PrintWriter(f);
		} catch (FileNotFoundException e) {
		}
		
		for (SnowDay d : days){
			writer.write(d.toString());			
			}
		writer.close();	
		
	}
	
	/**
	 * Sorts the linked list
	 */
	public void sort(){
		Collections.sort(days);
		
	}
	 
	/**
	 * Method to get the total days from the first day entered
	 * @return The total days
	 */
	public int totalDays() {
		int time = 0;
		if(days.size() > 0){
			//Gets total time as miliseconds and divides by total miliseconds in a day
		time =(int) (((days.getLast().getDate().getTime() - days.getFirst().getDate().getTime()) / 86400000 + 1));
		}
		return time;
	}

}
