/**
 * Andrew Markiewicz's HW3. Meant to recreate Bouncy
 */

//imports needed external packages
import java.awt.*;
import animator.Animated;
import animator.ClickListener;

public class Scene extends Animated {
	public void startup(){

		//Starts up the Ball
		Ball b1 = new Ball();
		animator.include(b1);
		animator.setClickListener(b1);

	}
}

class Ball extends Animated implements ClickListener	{

	//Set instance variables
	private final int maxVelocity = 10;		//Declares and sets max & min for Vel & Dimensions
	private final int maxWidth = 40;
	private final int minWidth = 10;
	private int xVel, yVel;	//Declares velocity, position, and width variables and click pos
	private int width = minWidth +  (int)(Math.random()*(maxWidth-minWidth));	//declares and sets value for width between min and max
	private float colorVal = (float)(Math.random()*10);		//declares and sets color value for random
	private int x;
	private int y;
	Ball next;

	//generates random int's for velocity between min and max Vel
	public int randomVelocity()	{
		return -maxVelocity + (int)(Math.random()*(2*maxVelocity));

	}

	//creates random values for starting position and velocity, and sets them to instance variables

	public void startup(){
		y = (int)(Math.random()*animator.getSceneHeight() - width); //set random starting position
		x = (int)(Math.random()*animator.getSceneWidth()) - width;
		xVel = randomVelocity();
		yVel = randomVelocity();


	}

	//takes 2 ints and find the distance from them to the center of the ball using pathagreans(sp???) therom
	public double distanceFrom(int xCo, int yCo){

		int xd = xCo - (x+(width/2));
		int yd = yCo - (y+(width/2));

		return Math.sqrt(xd*xd + yd*yd);
	}	

	
	
	//Click method that listens for a click, and creates a new ball if the click is within the ball	
	private boolean ifnext = false;
	public void click(int x, int y){
		/*I know that I need to make sure that the balls do not lose reference to other balls, but I cannot figure out
		 * how to impliment that
		 */
		
		
		if(ifnext==true){
			next.click(x, y);
			ifnext = true;
		}


		if(distanceFrom(x,y)<=(width/2)){
			next = new Ball();
			animator.include(next);
			ifnext = true;

		}	

	}



	public void draw()	{
		//sets color for balls && background
		animator.setBackgroundColor(Color.BLACK);
		screen.setColor(Color.getHSBColor(colorVal, colorVal, colorVal));

		//draws ball and gives it a velocity
		screen.fillOval(x, y, width, width);
		x = x + xVel;
		y = y + yVel;

		//Does the bouncing off the wall effects. Width/2 used to compensate for the width of the oval. not do bouning from the middle of the oval
		if(x + width >= animator.getSceneWidth()){
			x = animator.getSceneWidth() - width ;
			xVel = -xVel;			
		}

		if(x <= 0){
			x =0;
			xVel = -xVel;	
		}

		if (y + width >= animator.getSceneHeight()){
			y = animator.getSceneHeight() - width;
			yVel = -yVel;
		}

		if(y <= 0){
			y = 0;
			yVel = -yVel;
		}
	}


}