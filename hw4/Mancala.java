import java.awt.*;

import animator.Animated;
import animator.ClickListener;

public class Mancala extends Animated {

	//Create instance variables that will be written to by user
	private int numHouses;
	private int numStones;

	public void startup() {
		//Asks for input for number of houses per row. Keeps asking till in range
		animator.write("Enter the number of houses per row (4-10)");
		numHouses = animator.readInt();
		while(numHouses < 4 || numHouses > 10){
			animator.write("Enter the number of houses per row (4-10)");
			numHouses = animator.readInt();
		}

		//Asks for input for number of stones per house
		animator.write("Enther the number of stones begining in each house (3-10)");
		numStones = animator.readInt();
		while(numStones < 3 || numStones > 10){
			animator.write("Enther the number of stones begining in each house (3-10)");
			numStones = animator.readInt();			
		}

		Board board = new Board(numHouses, numStones);  //Creates new board object, remember to add arguments for constructor
		animator.include(board);
		animator.setClickListener(board);  //sets click listener to board object
	}
}



class Board extends Animated implements ClickListener {

	//Creates instance variables
	private Houses[] houses0;
	private Houses[] houses1;
	private Stores[] stores;
	private int numHouses, numStones, xClick = -1, yClick = -1; 




	//Startup method to include houses and stores in animator
	public void startup(){

		//Declares array Sizes
		houses0 = new Houses[numHouses];
		houses1 = new Houses[numHouses];
		stores = new Stores[2];	

		//Calls makeObjects method so objects are created
		makeObjects();


		//Includes all my Houses in animator
		for(int i = 0; i < houses0.length; i++){
			animator.include(houses0[i]);
			animator.include(houses1[i]);
		}

		//Includes all my stores in animator
		for(int i = 0; i <= 1; i++){
			animator.include(stores[i]);

			//runs sendpos method to send the houses their "house number" and the xClick, yClick	


		}
		sendPos();	

	}

	//'encodes' the starting index for the addWidthDirection method
	public int encodeSpot(int index, int row){
		return numHouses  * row + index;
	}

	//j is the index of all the stores&& houses from 0 -> 2 * numhouses + 1
	//then adds to the store if at the end of the row
	public void addWithDirection( Stones[] s, int start){
		int j = start;
		for(int i = 0; i < s.length; i++){
			j = (j + 1) % (2 * numHouses + 2);
			if( s[i] == null)
				return;
			if(j < numHouses){
				houses0[j].addStone(s[i]);
			}
			else
				if(j == numHouses){
					stores[1].addStone(s[i]);
				}
				else 
					if(j <= 2 * numHouses){
						houses1[2 * numHouses - j].addStone(s[i]);
					}
					else
						stores[0].addStone(s[i]);	
		}
	}


	//Click method. x&y Click set to x&y coor
	public void click(int x, int y) {		
		xClick = x;
		yClick = y;

		for(int i = 0; i < numHouses; i++){
			houses0[i].setClick(xClick, yClick);
			houses1[i].setClick(xClick, yClick);

		}	

		for(int z = 0;  z < numHouses; z++){
			if(houses0[z].ifClicked()){
				Stones[] s = houses0[z].removeStones();
				addWithDirection(s, encodeSpot(z, 0));

			}
		}

		for(int z = 0;  z < numHouses; z++){
			if(houses1[z].ifClicked()){
				Stones[] s = houses1[z].removeStones();
				addWithDirection(s, encodeSpot(z, 1));

			}
		}




	}



	//Creates a constructor that accepts two ints.

	public Board(int x, int y){
		numHouses = x;
		numStones = y;

	}




	//Method to make objects. Creates them and points array elements to them
	public void makeObjects()	{


		//makes house objects
		for(int i = 0; i < houses0.length; i++){
			houses0[i] = new Houses(numHouses, numStones);
			houses1[i] = new Houses(numHouses, numStones);
			houses1[i].ifBottom(true);
		}

		//Matkes both of the store objects and points array elememnts to them
		//store1 is right store so send true
		stores[0] = new Stores(false, numHouses, numStones); 
		stores[1] = new Stores(true, numHouses, numStones);

	}	


	public void draw(){


	}


	//Method to send 'house number' to each house.
	public void sendPos(){

		for(int i = 0; i < numHouses; i++){
			houses0[i].setNums(i);
			houses1[i].setNums(i);

		}
	}

}


class Houses extends Animated{

	//Create instance variables
	private int xScreenMid, yScreenMid, widthBet, houseWidth, numHouses, houseNum;
	private int xClick, yClick, numIniStones, screenWidth, screenHeight, xPos, yPos, currentCount;
	private Stones[] stonesIn;
	private boolean ifBottom = false, flag = true;


	//Constructor that receives the click coords, number of houses, and number of stones initially in each house
	//Also creates array
	//Also calls the make initial stones method
	public Houses(int z, int q){

		numHouses = z;	
		numIniStones = q;

		//Sets the stones-in array to the max number of stones in the game
		stonesIn = new Stones[(numIniStones * numHouses * 2)]; 

		//Call the makeIniStoneObjects method
		makeIniStoneObjects();


	}


	//Makes the actual stone objects. Just enough for the initial amount
	//Fills the rest with null objects
	public void makeIniStoneObjects(){

		for(int i = 0; i < numIniStones; i++){
			stonesIn[i] = new Stones();

		}
		currentCount = numIniStones;

		for(int i = numIniStones + 1; i < (numIniStones * numHouses * 2); i ++){
			stonesIn[i] = null;
		}
	}

	//sets the house number to a instance variable
	public void setNums(int z){
		houseNum = z;
	}

	//return house number
	public int retNum(){
		return houseNum;
	}

	//return stonesIn length
	public int retLength(){
		int count =0;
		for(int i = 0; i < stonesIn.length; i++ ){
			if(stonesIn[i] != null){
				count += 1;
			}
		}
		return currentCount;
	}

	//takse click variables and sets them to x and y click
	public void setClick(int x, int y){

		xClick = x;
		yClick = y;
	}

	//recieves and sets the boolean to see if it is the bottom row of houses
	public void ifBottom(boolean bottom){
		ifBottom = bottom;
	}


	//draws houses along with doing a lot of other stuff
	public void draw(){



		//sets variables that are easier to write for screen height and width
		int screenHeight = animator.getSceneHeight();
		int screenWidth = animator.getSceneWidth();

		//sets width between and color
		widthBet = 7;
		screen.setColor(Color.BLACK);

		//Set height of houses depending on either height of screen or width. Draws with lesser of two
		int diamDepWidth = (int) ((screenWidth - widthBet * numHouses) * ( 0.6 / numHouses) );
		int diamDepHeight = (int) (screenHeight * (0.3));

		if(diamDepWidth < diamDepHeight){

			houseWidth = diamDepWidth;			
		}

		else{
			houseWidth = diamDepHeight;
		}


		//set xPos for the first row
		xPos =(int) (screenWidth * 0.2 + houseNum * widthBet + houseNum * houseWidth - 0.5 * houseWidth);

		//draws house if on the bottom row
		if(ifBottom){
			for(int i = 0; i <= numHouses; i++ )	{			
				if(houseNum == i ){			
					yPos =(int) (screenHeight * 0.5 + widthBet);
				}					
			}

			//draws actual ovals
			screen.fillOval(xPos, yPos, houseWidth, houseWidth);

			//draws stone #
			screen.setColor(Color.WHITE);
			screen.drawString(getNumStones(), (int) ( xPos + 0.5 * houseWidth), (int) (yPos + 0.5 * houseWidth));
			//screen.setColor(Color.BLACK);
		}

		//draws the top row
		else{

			for(int i = 0; i <= numHouses; i++ )	{			
				if(houseNum == i ){			
					yPos =(int) (screenHeight * 0.5 - houseWidth);
				}					
			}

			//draws actual ovals
			screen.fillOval(xPos, yPos, houseWidth, houseWidth);

			//draws stone #
			screen.setColor(Color.WHITE);
			screen.drawString(getNumStones(), (int) ( xPos + 0.5 * houseWidth), (int) (yPos + 0.5 * houseWidth));
			//screen.setColor(Color.BLACK);
		}




		//'gives' the Stores and Stones class the housewidth
		Stores.getHouseWidth(houseWidth);

		for(int i = 0; i < stonesIn.length; i++)
			if(stonesIn[i] != null){

				stonesIn[i].getHouseInfo(houseWidth, xPos, yPos);

			}

		if(flag)
			for(int i = 0; i < stonesIn.length; i++){
				if(stonesIn[i] != null){
					animator.include(stonesIn[i]);
				}
				flag = false;
			}

	}

	//Method to remove stones from the end of the stonesIn array
	public Stones[] removeStones(){

		//makes count it the number of stones in house
		Stones[] result = new Stones[stonesIn.length];
		for(int i = 0; i < stonesIn.length; i++ ){
			if(stonesIn[i] != null){
				result[i] = stonesIn[i];
				animator.remove(stonesIn[i]);
				stonesIn[i] = null;
				currentCount--;
			}
		}
		return result;




	}


	//Method to add stones to the end of the stonesIn array
	public void addStone(Stones s){
		stonesIn[currentCount++] = s;
		animator.include(s);
	}

	//Method to get the current number of stones in the stonesIn array
	//outputs a string
	public String getNumStones(){
		int count =0;
		for(int i = 0; i < stonesIn.length; i++ ){
			if(stonesIn[i] != null){
				count += 1;
			}
		}
		return "" + count;
	}

	//method to check for a click inside the house
	public boolean ifClicked(){

		if( xPos <= xClick && xClick <= xPos + houseWidth && yClick >= yPos && yClick <= yPos + houseWidth  ){
			xClick = -1;
			yClick = -1;
			return true;
		}
		else{
			return false;

		}

	}


}


class Stones extends Animated{
	private float colorVal1 = (float)(Math.random()*10);		
	private float colorVal2 = (float)(Math.random()*10);
	private float colorVal3 = (float)(Math.random()*10);
	private int widthBet = 7, xPos, yPos, houseX, houseY;
	private double xRel, yRel, stoneWidth, stoneHeight;


	private double randX, randY;
	public Stones(){

		//random numbers between -1 + 1	
		randX = Math.random() * 2 - 1.0;
		randY = Math.random() * 2 - 1.0;

		//uses the distance formula to find a relative position within houses
		xRel = randX * Math.abs(randX) / Math.sqrt(randX*randX + randY*randY  + 0.0000001);
		yRel = randY * Math.abs(randY) / Math.sqrt(randX*randX + randY*randY + 0.0000001);



	}

	static private int houseWidth;
	//declares and sets color value randomly

	public void getHouseInfo(int x, int p, int q){
		houseWidth = x;
		houseX = p;
		houseY = q;
	}



	public void draw()	{		

		//use for width/height 
		if(animator.getSceneWidth() < animator.getSceneHeight()){
			stoneWidth = animator.getSceneWidth() * 0.015;
			stoneHeight = animator.getSceneWidth() * 0.025;
		}
		else{
			stoneWidth = animator.getSceneHeight() * 0.015;
			stoneHeight = animator.getSceneHeight() * 0.025;
		}

		//set x and y posision based on relative positions - scaling due to house size
		xPos = (int) (houseWidth * xRel / 2.2 + (houseWidth - stoneWidth) / 2.0 + houseX);
		yPos = (int) (houseWidth * yRel / 2.2 + (houseWidth - stoneHeight) / 2.0 + houseY);




		//sets color based on random colorVal
		screen.setColor(Color.getHSBColor(colorVal1, colorVal2, colorVal3));
		screen.fillOval(xPos,yPos, (int) (stoneWidth), (int) (stoneHeight));


	}



}

class Stores extends Animated{

	//Creates instance variables
	private int storeWidth, numHouses, numIniStones, currentCount;
	static int houseWidth;	
	private boolean right;
	//Creates array for # of stones in
	private Stones[] stonesIn;



	//Constructor that sets the store to be the right one 
	public Stores(boolean ifRightMost, int x, int z){
		numIniStones = z;
		numHouses = x;


		//creates stones objects in stonesIn array
		stonesIn = new Stones[(numIniStones * numHouses * 2)]; 



		if(ifRightMost){
			right = true;
		}	

	}


	static public void getHouseWidth(int x){
		houseWidth = x;
	}


	//Draws the stores
	public void draw()	{

		screen.setColor(Color.GRAY);

		int screenHeight = animator.getSceneHeight();
		int screenWidth = animator.getSceneWidth();

		//Set height of houses dep on either height of screen or width.
		int diamDepWidth = (int) (screenWidth * ( 0.15));
		int diamDepHeight = (int) (screenHeight * (0.35));

		//Chooses witch is smaller
		if(diamDepWidth < diamDepHeight){

			storeWidth = diamDepWidth;			
		}

		else{
			storeWidth = diamDepHeight;
		}


		//Draws the right store
		if(right){
			int widthBet = 7;
			int i = numHouses;
			screen.fillOval(((int) (screenWidth * 0.8)),(int) (screenHeight / 2  - 0.5 * storeWidth),  storeWidth, storeWidth );
			//draws # of stones in the center of the right store
			screen.setColor(Color.WHITE);
			screen.drawString(getNumStones(),(int)((screenWidth * 0.8) + (0.5 * storeWidth)),(int) (screenHeight / 2));
			screen.setColor(Color.BLACK);


		}

		//draws the left store
		else {

			screen.fillOval(0,(int) (screenHeight / 2 - 0.5 * storeWidth), storeWidth, storeWidth);

			//draws # of stones in the center of the left stone
			screen.setColor(Color.WHITE);
			screen.drawString(getNumStones(),(int)(.5 * storeWidth),(int) (screenHeight / 2));
			screen.setColor(Color.BLACK);



		}


	}


	//Method to remove the last stone in the stonesIn array
	public void removeStones(int number){



	}


	//Method to add stones to the end of the stonesIn array
	public void addStone(Stones s){
		stonesIn[currentCount++] = s;
		animator.include(s);
	}


	public String getNumStones(){
		int count =0;
		for(int i = 0; i < stonesIn.length; i++ ){
			if(stonesIn[i] != null){
				count += 1;
			}
		}
		return "" + count;
	}




}
