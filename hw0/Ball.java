/**
 * Simple demo of an animator using a Ball
 * 
 * @author Andrew Markiewicz
 */


import java.awt.*;
import animator.Animated;


public class Ball extends Animated {
	public void draw(int x, int y){
		screen.setColor(Color.BLUE);
		screen.fillOval(x, y, 40, 40);
		
	}

}
