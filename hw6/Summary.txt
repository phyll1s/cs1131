Andrew Markiewicz's summary file
--------------------------------
What I learned/observed from black box testing:

When blackbox testing I realized that it is true; it would take a lot to go through at test every path possible. Black box testing seems 
useful if you want to take a long time and play through everything. It might be useful compared to white box testing though, as when you white box test you only check for errors that you think are there. If you black box you run through the program how you think it should work, and you find bugs that normal users would find.



What I learned/observed from reviewing others code:

The usefulness of a the do/while loop
Commenting more, and commenting each part of a logic sequence so someone can follow it clearly. 
Just some small things here and there - different ways to do things. 
I need to think more abstractly.




What I learned/observed from having my code reviewd:

Use if/else instead of many if's
Remember my oppening comment block






Comments:

Man I'm glad we're done with animator. While I know that it is a good interface to get new programmers to recognize objects and 'show'
object interaction, it really is a pain...