/**
 * Mancala Implementation - Semi-Final
 *
 * This is my *almost* finished implimentation of Mancala. 
 * 
 * @author Andrew Markiewicz
 */


	//KJWALTER Commentor Kevin's overall notes on code - in addition to the couple of comments I've made throughout your actual code,
	// I'd like to summarize with saying that overall your code is very concise, clear, and elegant - how much of that is because
	// it's actually Bill's code, who knows, but nevertheless it's good coding style. With a few noted exceptions, commenting
	// effectively illuminates the workings of a function or the purpose of variables. Blackbox testing cases are at the bottom
    // of your code


import java.awt.*;
import java.util.*;
import animator.Animated;
import animator.ClickListener;

// MJJOHNSO: Needs an opening comment block.

public class Mancala extends Animated {

	//Create instance variables that will be written to by user
	private int numHouses;
	private int numStones;

	public void startup() {
		//Asks for input for number of houses per row. Keeps asking till in range
		animator.write("Enter the number of houses per row (4-10)");
		numHouses = animator.readInt();
		while(numHouses < 4 || numHouses > 10){	//MJJOHNSO: It would be possible to use a do-while loop here, but this works equally well.
			animator.write("Enter the number of houses per row (4-10)");
			numHouses = animator.readInt();
		}

		//Asks for input for number of stones per house
		animator.write("Enther the number of stones begining in each house (3-10)");
		numStones = animator.readInt();
		while(numStones < 3 || numStones > 10){
			animator.write("Enther the number of stones begining in each house (3-10)");
			numStones = animator.readInt();			
		}

		Board board = new Board(numHouses, numStones);  //Creates new board object, remember to add arguments for constructor
		animator.include(board);
		animator.setClickListener(board);  //sets click listener to board object
	}
}



class Board extends Animated implements ClickListener {

	/* 
	 * Contants used to draw the board
	 * These describe the board in "fundamental units"
	 *
	 */

// KJWALTER- I like how descriptive your comments are over what the instance variables
	// are, and especially when they're initialized like with unitWidth and unitHeight


	private final int borderWidth =  2;    
	private final int houseWidth  =  10;    
	private final int storeWidth  =  21;    

	//Boolean for who's turn
	//true = player 1's turn
	private boolean firstPlayer = true;

	// Overall width and height in units 
	// (can't be determined until number of houses is known)
	private final int unitWidth;    
	private final int unitHeight;    

	// Vector indices for each player
	private final int PLAYER_ONE = 0;
	private final int PLAYER_TWO = 1;

	// Create vector for stores
	private Vector<Store> stores;    

	//creates references to the movement vectors
	private Vector<Pit> player1s;
	private Vector<Pit> player2s;

	// One row (vector) of houses per player
	// 1st dimension is player, 2nd is respective house.
	// hosues[PLAYER_ONE][0] is the 1st house of player one.
	private Vector<Vector<House>> houses;

	/** 
	 * Construct a board with the specified houses and seeds per house.
	 *
	 * @param numHouses       The number of houses
	 * @param seedsPerHouse   The number of seeds per house initially
	 *
	 */ 
	Board(int numHouses, int seedsPerHouse) {   

		// Determine the overall board height in units
		unitHeight = Math.max(2*borderWidth+storeWidth,    // Vert space for stores & borders
				3*borderWidth+2*houseWidth); // Vert space for houses & borders

		// Determine the overall board width in units
		unitWidth =   (numHouses+3)*borderWidth   // Horiz space for borders
		+ numHouses*houseWidth        // Horiz space for houses
		+ 2*storeWidth;               // Horiz space for stores

		// Upper limit on seeds: The total number of seeds on the board
		int maximumSeeds = 2*numHouses*seedsPerHouse;

		// Create outter Vectors
		stores = new Vector <Store> ();
		houses = new Vector <Vector<House>> ();

		//Create inner vectors
		houses.add(new Vector<House> ());
		houses.add(new Vector<House> ());

		//Creates placement vectors
		player1s = new Vector <Pit> ();
		player2s = new Vector <Pit> ();

		stores.add(new Store(maximumSeeds));
		stores.add(new Store(maximumSeeds));

		// Create the houses for each player
		for(int i=0; i<numHouses ;i++) {
			houses.elementAt(PLAYER_ONE).add(new House(seedsPerHouse, maximumSeeds));
			houses.elementAt(PLAYER_TWO).add(new House(seedsPerHouse, maximumSeeds));
		}

		// KJWALTER- I like how clear and concise your add methods are and the accurate comments
		// describing the functionality of the code

		//// player one vector to add to \\\\\

		//the houses that belong to player 1 are adding from left to right
		for(int i=0; i<numHouses; i++){
			player1s.add(houses.elementAt(PLAYER_ONE).elementAt(i));		
		}

		//adds player one's store to the vector
		player1s.add(stores.elementAt(0));

		//adds player2's houses to player one's vector
		for(int i=houses.elementAt(PLAYER_TWO).size() - 1; i>=0; i-- ){
			player1s.add(houses.elementAt(PLAYER_TWO).elementAt(i));
		}


		//// player two vector to add to \\\\\


		//the houses that belong to player two going from right to left
		for(int i=houses.elementAt(PLAYER_TWO).size() - 1; i>=0; i-- ){
			player2s.add(houses.elementAt(PLAYER_TWO).elementAt(i));
		}		

		//adds the stores at the end of the vectors		
		player2s.add(stores.elementAt(1));


		//adds player1's houses to player two's vector	
		for(int i=0; i<numHouses; i++){					
			player2s.add(houses.elementAt(PLAYER_ONE).elementAt(i));
		}






	} // End Board(int, int)

	/**
	 * Include all of the sub-objects at startup time and set 
	 * this object to listen for clicks
	 */
	public void startup() {       
		// include all of the 
		animator.include(stores.elementAt(PLAYER_ONE));
		animator.include(stores.elementAt(PLAYER_TWO));
		for(int i=0;i<houses.elementAt(PLAYER_ONE).size();i++) {
			animator.include(houses.elementAt(PLAYER_ONE).elementAt(i));
			animator.include(houses.elementAt(PLAYER_TWO).elementAt(i));
		}

		// Ensure that this object (the board) listens for clicks
		animator.setClickListener(this);
	} // End startup()


	/**
	 * Update dimensions on hosues and stores and draw the player labels.
	 */
	public void draw() {

		// The number of pixels per unit depends on how things fit on the screen
		// Use the minimum of the potential "stretch" in each direction
		double pixelsPerUnit = Math.min(animator.getSceneWidth()/(double)unitWidth,
				animator.getSceneHeight()/(double)unitHeight);

		int borderPixels = (int)(borderWidth*pixelsPerUnit);

		// Compute the start x,y of the drawable area.
		// (This is the start of a "sub-window" into the real screen
		//  most things will be drawn relative to this offset)
		int xStart = (int)(animator.getSceneWidth()-unitWidth*pixelsPerUnit)/2;
		int yStart = (int)(animator.getSceneHeight()-unitHeight*pixelsPerUnit)/2;

		int horizOffset;    // Re-used for horizonal offsets from xStart
		int vertOffset;     // Re-used for vertical offsets from yStart
		int playerOneVert;  // The y coordinate of player one's houses
		int playerTwoVert;  // The y coordinate of player two's houses
		int width;          // Object width in pixels

		/*
		 * Player Two's store is inset by both borders
		 * Note: all arguments are in units of pixels
		 */
		horizOffset = (int) (  borderPixels                   // Immediately in the border
				+ storeWidth/2.0*pixelsPerUnit); // Offset to center
		vertOffset =  (int)  (  (unitHeight)/2.0              // Centered vertically  
				* pixelsPerUnit);               // in Pixels
		width = (int) (storeWidth*pixelsPerUnit);             // Width in pixels
		stores.elementAt(PLAYER_TWO).setCenter(xStart+horizOffset,  
				yStart+vertOffset);
		stores.elementAt(PLAYER_TWO).setWidth(width);
		stores.elementAt(PLAYER_TWO).setPixelsPerUnit(pixelsPerUnit);

		/*
		 * Player One's store is inset by the entire board.
		 * It's easiest to compute starting from the right 
		 * and working backwards.
		 */
		horizOffset = (int) (  unitWidth*pixelsPerUnit - borderPixels   // Immeadiately in the border
				- storeWidth/2.0 * pixelsPerUnit);         // Offset to center

		// Note: Uses same width and vertOffset as PLAYER_TWO's house
		stores.elementAt(PLAYER_ONE).setCenter(xStart+horizOffset,  
				yStart+vertOffset);
		stores.elementAt(PLAYER_ONE).setWidth(width);
		stores.elementAt(PLAYER_ONE).setPixelsPerUnit(pixelsPerUnit);

		// Setup the houses   
		width = (int) (houseWidth*pixelsPerUnit);                       // House width in pixels
		playerOneVert = (int) ( yStart+unitHeight/2.0*pixelsPerUnit     // Center of vert
				+ (borderWidth/2.0+houseWidth/2.0)      // + (1/2 border + 1/2 house
				* pixelsPerUnit);                 //    in pixels)
		playerTwoVert = (int) ( yStart+unitHeight/2.0*pixelsPerUnit     // Center of vert
				- (borderWidth/2.0+houseWidth/2.0)      // - (1/2 border + 1/2 house
				* pixelsPerUnit);                 //    in pixels)

		for(int i=0;i<houses.elementAt(PLAYER_ONE).size();i++) {
			horizOffset =  (int) ((storeWidth +                         // Border offset         
					2*borderWidth +                  
					houseWidth/2.0
					+ i*(houseWidth+borderWidth))*pixelsPerUnit);   // Offset to center of i-th house
			houses.elementAt(PLAYER_ONE).elementAt(i).setCenter(xStart+horizOffset,
					playerOneVert);
			houses.elementAt(PLAYER_ONE).elementAt(i).setWidth(width);
			houses.elementAt(PLAYER_ONE).elementAt(i).setPixelsPerUnit(pixelsPerUnit);

			houses.elementAt(PLAYER_TWO).elementAt(i).setCenter(xStart+horizOffset,
					playerTwoVert);
			houses.elementAt(PLAYER_TWO).elementAt(i).setWidth(width);
			houses.elementAt(PLAYER_TWO).elementAt(i).setPixelsPerUnit(pixelsPerUnit);
		}

		// Setup the labels
		screen.setColor(Color.black);
		horizOffset = (int)(xStart+unitWidth/2.0*pixelsPerUnit)-38;
		screen.drawString("Player Two", horizOffset, yStart+4);
		screen.drawString("Player One", horizOffset, 
				(int)(yStart+unitHeight*pixelsPerUnit) + 3);
		if(firstPlayer)
			screen.drawString("Player 1's Turn", 50, 50);
		else
			screen.drawString("Player 2's Turn", 50, 50);



		//Find if game is over and display winner	
		int totalR1 = 0;
		for(int i = 0; i < houses.elementAt(PLAYER_ONE).size(); i++){
			totalR1 += houses.elementAt(PLAYER_ONE).elementAt(i).getNumStones();
		}

		int totalR2 = 0;
		for(int i = 0; i < houses.elementAt(PLAYER_ONE).size(); i++){
			totalR2 += houses.elementAt(PLAYER_ONE).elementAt(i).getNumStones();
		}


		if(totalR1 == 0 || totalR2 == 0){ 	//MJJOHNSO: I like this method of determining the winner.  Much better than mine.

			if(stores.elementAt(0).getNumStones() > stores.elementAt(1).getNumStones())
				screen.drawString("Player 1 wins", 300, 50);
			if(stores.elementAt(0).getNumStones() < stores.elementAt(1).getNumStones())
				screen.drawString("Player 2 wins", 300, 50);
			if(stores.elementAt(0).getNumStones() == stores.elementAt(1).getNumStones())
				screen.drawString("Tie game", 150, 50);




		}

	} // End draw()






	public void click(int x, int y) {




		//Take firstplayer's turn
		if(firstPlayer) {
			int clickedHouse;
			Vector <Stone> stonesRemoved = new Vector<Stone>();
			for(int i = 0; i < houses.elementAt(PLAYER_ONE).size(); i++){

				if(houses.elementAt(PLAYER_ONE).elementAt(i).clicked(x, y) && player2s.elementAt(i).getNumStones() != 0){

					clickedHouse = i;
					int j = 0;
					stonesRemoved = houses.elementAt(PLAYER_ONE).elementAt(i).removeStones();
					for(; j< stonesRemoved.size(); j++){
						clickedHouse = (clickedHouse + 1)%player1s.size();
						player1s.elementAt(clickedHouse).addStone(stonesRemoved.elementAt(j));

					}


					//if the last stone lands in player 1's store
					if( player1s.elementAt(i + j) == stores.elementAt(0) ){
						screen.drawString("Go again Player 1", 500, 50); //MJJOHNSO: This is only going to be on screen for 1/10 of a second, so why bother?
						firstPlayer = true;
					}

					else{

					
						firstPlayer = false;
					}

					//if the last stone lands in the player's own empty house
					if((i + j) <= houses.elementAt(PLAYER_ONE).size() - 1 && houses.elementAt(PLAYER_ONE).elementAt(i + j).getNumStones() == 0 && houses.elementAt(PLAYER_TWO).elementAt(i + j).getNumStones() != 0 ){
						

						//Adds the stones from both houses to the captured vector
						Vector<Stone> captured = new Vector<Stone>();
						captured = houses.elementAt(PLAYER_TWO).elementAt(i + j).removeStones();
						Vector<Stone> toCapture = new Vector<Stone>();
						toCapture = (houses.elementAt(PLAYER_ONE).elementAt(i + j).removeStones());
						captured.add(toCapture.elementAt(0));

						stores.elementAt(0).addStones(captured, captured.size());

					}


				}

			}

		}



		//take secondplayer's turn
		if(firstPlayer == false){  //MJJOHNSO: Instead of using a second if statement, you could've just used an else.
			int clickedHouse;
			Vector <Stone> stonesRemoved = new Vector<Stone>();
			for(int i = 0; i < player2s.size() / 2; i++){

				if(player2s.elementAt(i).clicked(x, y) && player2s.elementAt(i).getNumStones() != 0){

					clickedHouse = i + 1;
					int j = 0;
					
					stonesRemoved = ((House) player2s.elementAt(i)).removeStones();
					for(; j< stonesRemoved.size(); j++){

						player2s.elementAt(clickedHouse).addStone(stonesRemoved.elementAt(j));
						clickedHouse = (clickedHouse + 1)%player2s.size();
					}


					//if the last stone lands in player 2's store
					if( player2s.elementAt(i + j) ==  stores.elementAt(1)){
						screen.drawString("Go again Player 2", 500, 50);
						firstPlayer = false;

					}

					else{
									
						firstPlayer = true;
					}

					//if the last stone lands in the player's own empty house
					if((i + j) < player2s.size()/2 && player2s.elementAt(i + j).getNumStones() == 0 && player1s.elementAt(i + j).getNumStones() != 0)  {
						

						//Adds the stones from both houses to the captured vector
						Vector<Stone> captured = new Vector<Stone>();
						captured = houses.elementAt(PLAYER_ONE).elementAt(houses.elementAt(PLAYER_ONE).size() - i - j - 1).removeStones();
						Vector<Stone> toCapture = new Vector<Stone>();
						toCapture = (houses.elementAt(PLAYER_TWO).elementAt(houses.elementAt(PLAYER_TWO).size() - i -j - 1).removeStones());
						captured.add(toCapture.elementAt(0));

						stores.elementAt(1).addStones(captured, captured.size());

					}


				}			

			}

		} // End click(int,int)
	}


} // End class Board



class Stone extends Animated {

	// Stone's have a particular location within a circle
	private double fromCenter;      // How far "out" on the radius (0..1)
	private double angle;           // The angle from 0
	private int    containerRadius; // Radius of the thing the stone is in
	private Color  color;
	private int    xCenter;
	private int    yCenter;
	private double pixelsPerUnit;

	// PerUnit dimensions of a stone
	private final int stoneWidth  =  2;    
	private final int stoneHeight =  1;

	//Makes a single stone
	public Stone() {
		// Chose random values for placement and color
		fromCenter = Math.random();
		angle = 2*Math.PI*Math.random();
		color = Color.getHSBColor((float)Math.random(), 1f, 1f);
	} // End Stone()


	//set the location of the containing object's center
	public void setContainerCenter(int x, int y) {
		xCenter=x;
		yCenter=y;
	} // End setContainerCenter(int,int)



	//set the radius of the container
	public void setContainerRadius(int r) {
		containerRadius=r;
	} // End setContainerRadiut(int)



	//set the pixels per unit of drawing area
	public void setPixelsPerUnit(double p) {
		pixelsPerUnit = p;
	} // End setPixelsPerUnit(double)



	//draw the stone
	public void drawStone() {
		int maxRadius = containerRadius - 
		(int) (Math.max(stoneWidth*pixelsPerUnit,
				stoneHeight*pixelsPerUnit));

		int width = (int) (stoneWidth*pixelsPerUnit);
		int height = (int) (stoneHeight*pixelsPerUnit);
		screen.setColor(color);

		screen.fillOval((int) (xCenter+Math.sin(angle)*fromCenter*maxRadius-width/2),
				(int) (yCenter+Math.cos(angle)*fromCenter*maxRadius-height/2),
				width, height);
	} // End drawStone()
}



//Extends Animated therefore we can use it in house and store class
class Pit extends Animated {


	protected int xCenter;      // x Center
	protected int yCenter;      // y Center
	protected int width;        // width
	protected Vector<Stone> stones;  // Array of stones
	protected int numStones=0;  // number of stones in array
	protected double pixelsPerUnit;



	// set the coordinates of the center
	public void setCenter(int x, int y) {
		xCenter = x;
		yCenter = y;
	} // End setCenter(x,y)



	//Set total width of the pit
	public void setWidth(int w) {
		width = w;
	} // End setWidth(int)



	//set the pixels per unit of drawing area
	public void setPixelsPerUnit(double p) {
		pixelsPerUnit = p;
	} // End setPixelsPerUnit(double)




	//get the number of stones in the pit
	public int getNumStones() {
		return numStones;
	} // End getNumStones()



	//add a single stone to the store
	public void addStone(Stone s) {
		stones.add(s);
	} // End addStone(Stone)

	//Method to see if clicked on
	public boolean clicked(int x, int y) {
		int xDiff = x-xCenter;
		int yDiff = y-yCenter;
		return Math.sqrt(xDiff*xDiff+yDiff*yDiff) < width/2;
	} // End clicked(int,int)



}





class Store extends Pit {

	Color color = new Color(120,45,45);

	//create a store capable of holding stones 
	Store(int maxStones) {
		stones = new Vector <Stone> (maxStones);
	} // End Store(int) 


	//add an entire array of stones to the store
	public void addStones(Vector <Stone> toAdd, int num) {
		for(int i=0;i<num;i++) {
			stones.add(toAdd.elementAt(i));
		}
	} // End addStones(Stone [],int)


	//Draw the Store
	public void draw() {
		int xStart = xCenter - width/2;
		int yStart = yCenter - width/2;

		// Draw the Store
		screen.setColor(color);
		screen.fillOval(xStart,yStart,width,width);

		for(int i=0;i<numStones;i++) {
			stones.elementAt(i).setContainerCenter(xCenter, yCenter);
			stones.elementAt(i).setContainerRadius(width/2);
			stones.elementAt(i).setPixelsPerUnit(pixelsPerUnit);

			// Set the "screen" and draw the stones
			stones.elementAt(i).setGraphics(screen);                
			stones.elementAt(i).drawStone();
		}

		// Show the number of stones
		numStones = stones.size();
		screen.setColor(Color.white);
		screen.drawString(""+numStones, xCenter, yCenter);
	} // End draw()


}
class House extends Pit {

	Color color = new Color(90,45,45);
	private int maxStones;

	//create a house capable of holding stones 
	House(int initStones, int maxSt) {
		maxStones = maxSt;
		stones = new Vector<Stone> ();
		for(int i=0;i<initStones;i++) {
			stones.add(new Stone());
		}

	} // End House(int,int) 

	public void draw() {
		int xStart = xCenter - width/2;
		int yStart = yCenter - width/2;

		// Draw the House
		screen.setColor(color);
		screen.fillOval(xStart,yStart,width,width);

		for(int i=0;i<stones.size();i++) {
			stones.elementAt(i).setContainerCenter(xCenter, yCenter);
			stones.elementAt(i).setContainerRadius(width/2);
			stones.elementAt(i).setPixelsPerUnit(pixelsPerUnit);
			stones.elementAt(i).setGraphics(screen);
			stones.elementAt(i).drawStone();
		}

		// Show the number of stones
		numStones = stones.size();
		screen.setColor(Color.white);
		screen.drawString(""+numStones, xCenter, yCenter);
	} // End draw()


	//Method to removes stones
	public Vector<Stone> removeStones() {

		Vector<Stone> retVal = stones;
		stones = new Vector<Stone> (maxStones);
		numStones = 0;
		return retVal;
	} // End removeStones



}


/* Blackbox Testing
 * Test # 1 - Click a house on either row to see if the stones are accurately distributed through the player's houses

Test # 2 - Click a house on either row such that according to Mancala's rules the stones should be placed in the following houses, in the player's store and 
then wrap around to the opposite row to see if the code follows Mancala's game logic accurately

Test # 3 - Click a house such that according to Mancala's game logic, the stones in the clicked house should be sequentially added to the houses and the 
player's store and the opposite row such that it'll go through the clicked player's row, the clicked player's house, the opposite row, the clicked 
player's row again, the player's house again and the opposite row again to check to see if the code accurately distributes large numbers of stones 
spanning multiple rows

Test # 4 - Click a house such that the last stone ends in an empty house with a house containing stones opposite it to see if the stones opposite the house 
where the last stone fell are accurately 'captured' and added to the correct player's store

Test # 5 - Click a house such that the last stone falls in a player's house to see if the code accurately detects that that player should have his turn repeat

 ---------------------------------------------------
//**Results of testing your code with above tests**\\
 ---------------------------------------------------

Test # 1 - Stones were accurately distributed through the correct amount of houses
Test # 2 - Stones were accurately distributed through the correct houses, correctly through the store and correctly into the opposite row
Test # 3 - The stones were accurately distributed - however, a curious anomaly with turn detection was turned up which is further described in Test # 5 
Test # 4 - The stones were accurately 'captured' and sent to the correct player's store
Test # 5 - It would appear that if the amount of stones is greater than the remainder of the clicked row, the store, and the opposite row then the player 
automatically gets to go again, regardless of whether or not the stone then lands in the player's house - other than in that particular scenario, it 
appears to perform as anticipated

*/