import java.util.ArrayList;
import java.util.Iterator;
import net.datastructures.EmptyQueueException;
import net.datastructures.Queue;
/**
 * A queue made from an array list
 * @author AJMARKIE
 *
 * @param <E> A generic
 */

public class ArrayListQueue<E> implements Iterable<E>, Queue<E> {

	public static void main(String[] args){
	}

	ArrayList<E> queue = new ArrayList();

	/**
	 * returns and removes the element at the front of the queue
	 */

	public E dequeue() throws EmptyQueueException {
		E temp = queue.get(0);
		queue.remove(0);
		return temp;
	}


	/**
	 * adds an element to the back of the queue
	 */
	public void enqueue(E e) {
		queue.add(e);

	}


	/**
	 * Views the front of the queue w/o removing it
	 */
	public E front() throws EmptyQueueException {
		return queue.get(0);
	}



	public boolean isEmpty() {
		if(queue.size()==0)
			return true;
		else
			return false;
	}



	public int size() {

		return queue.size();
	}

	public Iterator<E> iterator() {
		return queue.iterator();
	}

}
