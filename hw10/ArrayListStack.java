import java.util.ArrayList;
import java.util.Iterator;
import net.datastructures.EmptyStackException;
import net.datastructures.Stack;
/**
 * A stack made from an array list
 * @author AJMARKIE
 *
 * @param <E> A generic
 */

public class ArrayListStack<E> implements Iterable<E>, Stack<E> {

	public static void main(String[] args){
	}

	private ArrayList<E> stack = new ArrayList();

	/**
	 * returns and removes the top element
	 */
	public E pop(){
		E temp;

		temp = stack.get(stack.size()-1);

		return temp;
	}

	/**
	 * Adds to the top of the stack
	 */
	public void push(E e){
		stack.add(e);		
	}

	public boolean isEmpty() {
		if(stack.size() == 0)
			return true;
		else
			return false;

	}

	public int size() {

		return stack.size();
	}

	/**
	 * Views the top of the stack w/o removing
	 */
	public E top() throws EmptyStackException {

		return stack.get(stack.size()-1);
	}

	public Iterator<E> iterator() {

		return stack.iterator(); 
	}


}
