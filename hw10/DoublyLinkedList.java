import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * A doubly Linked List made from scratch
 * @author AJMARKIE
 *
 * @param <E> A generic
 */
public class DoublyLinkedList<E> implements Iterable<E> {

	public static void main(String[] args){
	}

	/**
	 * Inner Node class that holds one piece of information
	 * @author AJMARKIE
	 *
	 */
	private class Node{

		private Node next;
		private Node previous;
		private E element;

		public Node(E e, Node p, Node n){
			element = e;
			previous = p;
			next = n;
		}

		public void setElement(E e) {
			element = e; 
		}

		public E getElement(){
			return element;
		}


		public void setNext(Node e){
			next = e;
		}

		public void setPrevious(Node e){
			previous = e;
		}

		public Node getNext(){
			return next;
		}

		public Node getPrevious(){
			return previous;
		}

	}
	private Node head;
	private Node tail;

	/**
	 * Initializes the head and tail sentinal nodes
	 */
	public DoublyLinkedList(){
		head = new Node(null, null, null);
		tail = new Node(null, head, null);
		head.setNext(tail);

	}

	public int size(){
		int count=0;
		if(head.getNext() == tail)
			return 0;
		else
			for(Node temp = head.getNext() ; temp != tail; temp=temp.getNext()){
				count++;
			}
		return count;
	}

	public void addFirst(E e){
		Node toAdd = new Node(e, head, head.getNext());
		head.getNext().setPrevious(toAdd);
		head.setNext(toAdd);		
	}

	public void addLast(E e){
		Node toAdd = new Node(e, tail.previous, tail);
		tail.getPrevious().setNext(toAdd);
		tail.setPrevious(toAdd);		
	}

	public Node removeFirst(){
		Node temp = head.getNext();
		head.getNext().getNext().setPrevious(head);
		head.setNext(head.getNext().getNext());
		return temp;
	}

	public Node removeLast(){

		Node temp = tail.getPrevious();
		tail.getPrevious().getPrevious().setNext(tail);
		tail.setPrevious(tail.getPrevious().getPrevious());		
		return temp;		
	}

	/**
	 * returns the first element without removing it
	 * @return the first element
	 */
	public Node peekFirst(){
		return head.getNext();
	}

	/**
	 * Returns the last element without removing it
	 * @return the last element
	 */
	public Node peekLast(){
		return tail.getPrevious();
	}

	public String toString(){
		String toReturn = "";				
		if(head.getNext() == tail)
			return null;
		else
			//Starts athe the first element after the head, checks if it's the tail, runs through the body, and then set it to the next element
			for(Node temp = head.getNext() ; temp != tail; temp=temp.getNext()){
				toReturn = toReturn + temp.getElement().toString();
			}
		return toReturn;
	}

	public Iterator iterator() {

		DoublyLinkedListIterator iterator = new DoublyLinkedListIterator<E>(head);
		return iterator;
	}

	public class DoublyLinkedListIterator<E> implements Iterator{

		Node current;
		public  DoublyLinkedListIterator(Node c){
			if(c.getNext() == tail)
				current = c;
			else
				current = c.getNext();
		}


		public boolean hasNext() {

			if(current.getNext() != tail)
				return true;
			else
				return false;
		}

		public Object next() {
			try{
				return current.getNext();
			} catch(NoSuchElementException e) {
				return null;
			}
		}

		public void remove() {
			UnsupportedOperationException e = new UnsupportedOperationException("Did not implement");
			throw(e);
		}


	}


}