import net.datastructures.EmptyQueueException;
import net.datastructures.Queue;
import java.util.Iterator;
import java.util.LinkedList;
/**
 * A queue made from a linked list
 * @author AJMARKIE
 * 
 * @param <E> A generic
 */

public class LinkedListQueue<E> implements Iterable<E>, Queue<E> {

	public static void main(String[] args){
	}

	LinkedList<E> queue = new LinkedList();

	/**
	 * returns and removes the element at the front of the queue
	 */
	public E dequeue() throws EmptyQueueException {
		E temp = queue.getFirst();
		queue.removeFirst();
		return temp;
	}

	/**
	 * adds an element to the back of the queue
	 */
	public void enqueue(E e) {
		queue.add(e);
	}

	/**
	 * views the front element of the queue w/o removing it
	 */
	public E front() throws EmptyQueueException {

		return queue.getFirst();
	}

	public boolean isEmpty() {

		if(queue.size() == 0)
			return true;
		else
			return false;
	}

	public int size() {

		return queue.size();
	}

	public Iterator<E> iterator() {

		return queue.iterator();
	}

}
