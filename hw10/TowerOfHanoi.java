import java.io.IOException;
import java.util.Scanner;
/**
 * An implementation of the Tower of Hanoie problem
 * @author AJMARKIE
 *
 */

public class TowerOfHanoi {

	public static void main(String[] args) {
		TowerOfHanoi tower = new TowerOfHanoi();
		System.out.println("How many disks?");
		Scanner scanner = new Scanner(System.in);
		int numDisks = scanner.nextInt();
		System.out.println("Starting with " + numDisks + " disks on peg 1");  
		tower.hanoi(numDisks, "1", "3", "2");
	}

	/**
	 * Uses recursion to move through the tower problem
	 * @param n the number of disks
	 * @param from the original post
	 * @param to the final post
	 * @param temp the middle post
	 */
	public void hanoi (int n, String from, String to, String temp){
		if(n>0){
			hanoi(n-1, from, temp, to);

			System.out.println("Move disk " + n + " from " + from + " to " + to);
			hanoi(n-1, temp, to, from);
		}
	}
}
