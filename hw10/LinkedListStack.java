import java.util.Iterator;
import java.util.LinkedList;
import net.datastructures.EmptyStackException;
import net.datastructures.Stack;
/**
 * A stack made from a linked list
 * @author AJMARKIE
 *
 * @param <E> A generic
 */

public class LinkedListStack<E> implements Iterable<E>, Stack<E> {

	public static void main(String[] args){

	}

	LinkedList<E> stack = new LinkedList();

	/**
	 * returns and removes the element from the top of the stack
	 */
	public E pop(){

		E temp = stack.getLast();
		stack.removeLast();	
		return temp;
	}

	/**
	 * adds an element to the top of the stack
	 */
	public void push(E e){
		stack.addLast(e);
	}

	public Iterator<E> iterator() {
		return stack.iterator();
	}

	public boolean isEmpty() {
		if(stack.size()==0)
			return true;
		else
			return false;	
	}

	public int size() {

		return stack.size();
	}

	/**
	 * views the top of the stack w/o removing it
	 */
	public E top() throws EmptyStackException {
		return stack.getLast();
	}

}
